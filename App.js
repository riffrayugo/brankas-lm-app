/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import OneSignal from 'react-native-onesignal';
import { StatusBar, View } from 'react-native';
import { WebView } from 'react-native-webview';

export default class App extends Component {
  constructor(properties) {
    super(properties);
    OneSignal.init("b1f4be7d-4420-46fd-99b5-817000d35f71", { kOSSettingsKeyAutoPrompt: true });

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  render() {
    const url = 'https://www.brankaslm.com'
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="#dea637"
        />
        <WebView
          useWebKit={true}
          allowUniversalAccessFromFileURLs={true}
          domStorageEnabled={true}
          geolocationEnabled={true}
          javaScriptEnable={true}
          source={{ uri: url }}
          style={{ flex: 1 }}
        />
      </View>
    );
  }
}
